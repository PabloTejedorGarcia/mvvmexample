﻿using MVVMExample.ViewModel;
using System;
using System.Net.Sockets;

namespace MVVMConsoleExample
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Welcome to this proof of concept!");

            Console.WriteLine("What type of vacation do you want?");
            Console.WriteLine("1. Beach");
            Console.WriteLine("2. Mountain");
            Console.WriteLine("3. Interesting city");
            Console.WriteLine("4. I don't care");
            Console.WriteLine("5. Exit");

            string packet = Console.ReadLine();

            var travelAgency = new TravelAgencyVM();
            travelAgency.GeneratePackage(packet);

            Console.WriteLine(travelAgency.TravelPackage.Name);

        }
    }
}
