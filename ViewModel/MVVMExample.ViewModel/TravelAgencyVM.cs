﻿using MVVMExample.Model;
using MVVMExample.ViewModel.Factory;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVVMExample.ViewModel
{
    public class TravelAgencyVM
    {

        private GenericFactory m_genericFactory = GenericFactory.Instance;

        public TravelAgencyVM()
        {

        }

        public IPacket TravelPackage { get; private set; }

        public void GeneratePackage(string packet)
        {
            TravelPackage = m_genericFactory.CreatePacket(packet);
        }

    }
}
