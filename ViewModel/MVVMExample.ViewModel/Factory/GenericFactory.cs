﻿using MVVMExample.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVVMExample.ViewModel.Factory
{
    public class GenericFactory
    {
        // Implementation of two patterns: singleton and factory.
        /// <summary>
        /// Instance of generic factory.
        /// </summary>
        private readonly static GenericFactory m_instance = new GenericFactory();

        /// <summary>
        /// Private constructor.
        /// </summary>
        private GenericFactory()
        {
        }

        /// <summary>
        /// Instance
        /// </summary>
        public static GenericFactory Instance
        {
            get
            {
                return m_instance;
            }
        }

        /// <summary>
        /// Create package.
        /// </summary>
        /// <returns>Return package</returns>
        public IPacket CreatePacket(string type)
        {
            switch(type)
            {
                case "Beach":
                    return new Beach();
                case "City":
                    return new City();
                case "Mountain":
                    return new Mountain();
                default:
                    return new Packet();
            }
            return null;
        }
    }
}
}