﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVVMExample.Model
{
    /// <summary>
    /// Beach concrete implementation.
    /// </summary>
    public class Beach : Packet
    {
        /// <summary>
        /// Constructor of Beach.
        /// </summary>
        public Beach()
        {
            Name = "Visit to Asturias, paraíso verde";
            Time = "3 weeks";
        }

        /// <summary>
        /// Simple method of example.
        /// </summary>
        /// <returns>Example string</returns>
        public string Activities()
        {
            return "Cachopo, Cachopo, Cachopo";
        }
    }
}
