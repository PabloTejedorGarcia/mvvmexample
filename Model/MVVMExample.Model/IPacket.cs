﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVVMExample.Model
{
    /// <summary>
    /// IPacket interface
    /// </summary>
    public interface IPacket
    {

        /// <summary>
        /// Name property.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Time property.
        /// </summary>
        string Time { get; set; }

    }
}
