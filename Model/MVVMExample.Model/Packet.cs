﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVVMExample.Model
{
    /// <summary>
    /// Concrete implementation of Packet.
    /// </summary>
    public class Packet : IPacket
    {
        /// <summary>
        /// <see cref="IPacket"/>
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// <see cref="IPacket"/>
        /// </summary>
        public string Time { get; set; }

        /// <summary>
        /// Packet class constructor.
        /// </summary>
        public Packet()
        {
            Name = "Visit to the nearby";
            Time = "Two weeks of pure boring";
        }

    }
}
