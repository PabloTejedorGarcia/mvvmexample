﻿using System;


namespace MVVMExample.Model
{
    /// <summary>
    /// City concrete implementation.
    /// </summary>
    public class City : Packet
    {
        /// <summary>
        /// Constructor of City.
        /// </summary>
        public City()
        {
            Name = "Visit to León, cuna del parlamentarismo";
            Time = "3 weeks";
        }

        /// <summary>
        /// Simple method of example.
        /// </summary>
        /// <returns>Example string</returns>
        public string Monuments()
        {
            return "Cathedral, San Marcos and San Isidoro";
        }
    }
}
