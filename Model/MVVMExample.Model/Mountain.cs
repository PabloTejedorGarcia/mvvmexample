﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVVMExample.Model
{
    public class Mountain : Packet
    {
        /// <summary>
        /// Constructor of Mountain.
        /// </summary>
        public Mountain()
        {
            Name = "Visit to Asturias, paraíso verde";
            Time = "3 weeks";
        }

        /// <summary>
        /// Simple method of example.
        /// </summary>
        /// <returns>Example string</returns>
        public string Routes()
        {
            return "Picos de Europa, Curueño, Riaño";
        }
    }
}
